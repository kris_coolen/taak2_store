package be.vdab.store;

public class Bar extends ItemStore {

    /**************************
     ******CONSTRUCTORS********
     **************************/
    public Bar(){
        super(new String[]{"water","beer","coffee","thee","soda","salty chips"});
    };


    @Override
    public String getName() {
        return "The drunken shopper";
    }

    @Override
    public String getDescription() {
        return "lot's of nice beers and whiskeys";
    }
}

package be.vdab.store;

public interface ICart {

    public abstract boolean getFull();
    public abstract int getMaxItems();
    public abstract int getItemCount();
    public abstract void enterItem(String item);

}

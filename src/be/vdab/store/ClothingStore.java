package be.vdab.store;

public class ClothingStore extends ItemStore {



/**********************************************************************************************************************/

    /**************************
     ******CONSTRUCTORS********
     **************************/
    public ClothingStore(){
        super(new String[]{"red T-shirt", "blue jeans", "pair of socks", "leather jacket"});
    }

    @Override
    public String getName() {
        return "Calvin Klein";
    }

    @Override
    public String getDescription() {
        return "Fancy clothing";
    }
}

package be.vdab.store;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerTool {

    public Scanner keyboard = new Scanner(System.in);

    public String getNameFromScanner(){
        String name ="";
        while(true) {
            System.out.print("Enter your name: ");
            name = keyboard.nextLine();
            if (name.equals("")) {
                System.out.println("Please enter a name. Name cannot be an empty string.");
            }
            else break;//valid name
        }
        return name;
    }

    public int getAgeFromScanner(){
        int age;
        do {
            try {
                System.out.print("Enter your age: ");
                age = keyboard.nextInt();
                if (age < 0) System.out.println("Age cannot be negative. Please enter a non-negative age.");
                else break; //valid age
            } catch (InputMismatchException ime) {
                System.out.println("Please enter a (non-negative) integer number for age.");
            } finally {
                keyboard.nextLine();
            }
        }while(true);
        return age;
    }

    public Gender getGenderFromScanner(){
        String genderString="";
        while(true) {
          System.out.print("Enter a gender (M/F): ");
          genderString=keyboard.nextLine();
          if (genderString.equals("M")) {
              Gender gender = Gender.M;
              return gender;
          }
          if (genderString.equals("F")) {
              Gender gender = Gender.F;
              return gender;

          } else {// not a gender, so try again
                System.out.println("Please enter a valid age (M or F): ");
          }
        }
    }


}

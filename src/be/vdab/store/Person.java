package be.vdab.store;

public class Person implements IPerson {

    /*********************
     ******PROPERTIES*****
     *********************/
    private String name;
    private Gender gender;
    private ICart cart;
    private int age;
/**********************************************************************************************************************/

    /**************************
     ******CONSTRUCTORS********
     **************************/
    public Person(String name, int age, Gender gender){
        setName(name);
        setAge(age);
        setGender(gender);
        cart = new SmallCart();
    }
/**********************************************************************************************************************/

    /**************************
     ******SETTERS*************
     **************************/
    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setGender(Gender gender) {
        this.gender = gender;
    }


    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void setCart(ICart cart) {
        this.cart = cart;
    }
/**********************************************************************************************************************/

    /**************************
     ******GETTERS*************
     **************************/

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public ICart getCart() {
        return cart;
    }

    @Override
    public int getAge() {
        return age;
    }

/**********************************************************************************************************************/

    /**************************
     ******METHODS*************
     **************************/
    @Override
    public void walkToPlace(String name) {

        System.out.println(getName()+ " walks to " + name);
    }
}

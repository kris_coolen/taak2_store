package be.vdab.store;

public class ComputerStore extends ItemStore {

    /**************************
     ******CONSTRUCTORS********
     **************************/
    public ComputerStore(){
        super(new String[]{"Gaming computer", "Laptop 3000", "4K-Truecolor-Monitor"});
    };



/**********************************overrides of interface****************************************************/
    @Override
    public String getName() {
        return "Fresh Bytes";
    }

    @Override
    public String getDescription() {
        return "computers and stuff";
    }
}

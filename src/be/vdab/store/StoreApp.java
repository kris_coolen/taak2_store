package be.vdab.store;

import java.time.LocalDateTime;
import java.util.Random;

public class StoreApp {

    public static void main(String[] args) {

        ScannerTool tool = new ScannerTool();
        String name = tool.getNameFromScanner();
        int age = tool.getAgeFromScanner();
        Gender gender = tool.getGenderFromScanner();
        //create a person
        IPerson person = new Person(name,age,gender);
        IStore mall = new ShoppingCenter();
        startSimulation(person,mall);

    }

    public static void startSimulation(IPerson person, IStore mall ){

        //print date and time of now
        LocalDateTime nowDate = LocalDateTime.now();
        System.out.println(nowDate);
        //choose randomly from 3 weather conditions
        String[] conditions = {"It's a warm sunny day.", "It's a cold rainy day.", "It's a cold sunny day."};
        Random rand = new Random();
        System.out.println(conditions[rand.nextInt(3)]);

        mall.enterStore(person);

    }


}

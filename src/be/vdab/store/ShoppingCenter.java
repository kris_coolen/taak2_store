package be.vdab.store;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ShoppingCenter implements IStore
{
    private String name;
    private String description;

    private IStore[] stores = {new Bar(),new ClothingStore(), new ComputerStore() };


    /**************************
     ******GETTERS*************
     **************************/

    @Override
    public String getName() {
        return "Shopping Center 1 Genk";
    }

    @Override
    public String getDescription() {
        return "meeeee, veel winkels jong";
    }
/**********************************************************************************************************************/
    @Override
    public void enterStore(IPerson person) {
        System.out.println(person.getName() + " looks at " + this.getName());
        System.out.println("Without thinking" + (person.getGender()==Gender.M?" he":" she")+" decides to enter.");
        int choice = -2;
        while(choice!=-1) {
            showMenu();
            choice = getChoice();
            handleChoice(person, choice);
        }
    }

    protected void handleChoice(IPerson person, int choice){

        String place;
        if(choice==-1) {
            place = "exit of "+getClass().getSimpleName();
            person.walkToPlace(place);
            System.out.println(person.getCart().toString());

        }
        else{
            place=stores[choice].getClass().getSimpleName()+ ": " + stores[choice].getName();
            person.walkToPlace(place);
            stores[choice].enterStore(person);
        }
    }

    protected void showMenu(){
        System.out.println("List of stores: " + stores.length);
        for(int i=0; i<stores.length; i++){
            //System.out.println(i+". "+stores[i].getClass().getSimpleName()+": "+stores[i].getName());
            System.out.printf("%3d. %s: %s\n",i,stores[i].getClass().getSimpleName(),stores[i].getName());
        }
        System.out.printf("%3d. to exit shopping center\n",-1);
    }

    protected int getChoice(){
        System.out.print("Your choice: ");
        int choice;
        Scanner keyboard = new Scanner(System.in);
        while(true){
            try{
                choice = keyboard.nextInt();
                if(choice<-1 || choice>=stores.length){
                    System.out.println("Choose a number between 0 and "+(stores.length-1)+", or choose -1 to exit.");
                }
                else break;
            }
            catch(InputMismatchException ime){
                System.out.println("Choice must be a number. Try again.");

            }
            finally{
                keyboard.nextLine();
            }
        }
        return choice;
    }




}

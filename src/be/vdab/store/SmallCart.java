package be.vdab.store;

public class SmallCart implements ICart {

    /*********************
     ******FIELDS*********
     *********************/
    private static final int MAX_ITEMS = 10;
    private String[] itemList = new String[MAX_ITEMS];
    private boolean full = false;
    private int itemCount = 0;

    /*********************
     ******GETTERS*********
     *********************/

    @Override
    public int getMaxItems(){
        return MAX_ITEMS;
    }

    @Override
    public boolean getFull(){
        return full;
    }

    @Override
    public int getItemCount(){
        return itemCount;
    }


    /*********************
     ******METHODS*********
     *********************/
    @Override
    public void enterItem(String item) {
            if(full){
                System.out.println("Cart is full: "+ item + " does not fit into cart.");
            }
            else{

                itemList[itemCount++]=item;
                if(itemCount==MAX_ITEMS) full = true;
            }
    }

    @Override
    public String toString() {
        String items="";
        for(int i=0; i<itemCount; i++){
            items+="\n\t"+itemList[i];
        }
        return "Shopping cart items: " + itemCount +items;
    }
}

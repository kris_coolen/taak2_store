package be.vdab.store;

public interface IStore {

    public abstract String getName();
    public abstract String getDescription();

    public abstract void enterStore(IPerson person);

}

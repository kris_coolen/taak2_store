package be.vdab.store;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class ItemStore implements IStore {

    /*********************
     ******PROPERTIES*****
     *********************/
    protected String[] items;


/**********************************************************************************************************************/

    /**************************
     ******CONSTRUCTORS********
     **************************/
    public ItemStore(String[] items){
        this.items = items;
    }



/**********************************************************************************************************************/


    @Override
    public void enterStore(IPerson person) {
       // System.out.println(person.getName() + " walks to " + getClass().getSimpleName() + ": " + getName());
        int choice = -2;
        while(choice!=-1) {
            showMenu();
            choice = getChoice();
            handleChoice(person, choice);
        }

    }

    protected void showMenu(){
        System.out.println("Items for sale: " + items.length);
        for(int i=0; i<items.length; i++){
            System.out.printf("%3d. %s\n",i,items[i]);
        }
        System.out.printf("%3d. to exit store\n",-1);
    }

    protected void handleChoice(IPerson person, int choice){

        if(choice!=-1){
            System.out.println(person.getName() + " wants to buy " + items[choice]);
            if((person.getAge()<16) && (this instanceof Bar) && (choice==1)){
                System.out.println(person.getName() + ", you are too young for beer! Choose something else!");
            }else {
                System.out.println(person.getName() + " buys " + items[choice]);
                person.getCart().enterItem(items[choice]);
            }

        }
        else{
            person.walkToPlace("exit of "+getClass().getSimpleName()+": "+getName());
        }

    }

    protected int getChoice(){
        System.out.print("Your choice: ");
        int choice;
        Scanner keyboard = new Scanner(System.in);
        while(true){
            try{
                choice = keyboard.nextInt();
                if(choice<-1 || choice>=items.length){
                    System.out.println("Choose a number between 0 and "+(items.length-1)+", or choose -1 to exit store.");
                }
                else break;
            }
            catch(InputMismatchException ime){
                System.out.println("Choice must be a number. Try again.");

            }
            finally{
                keyboard.nextLine();
            }
        }
        return choice;
    }
}

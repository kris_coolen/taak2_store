package be.vdab.store;

public interface IPerson {

    public abstract String getName();
    public abstract void setName(String name);
    public abstract Gender getGender();
    public abstract void setGender(Gender gender);
    public abstract ICart getCart();
    public abstract void setCart(ICart cart);
    public abstract int getAge();
    public abstract void setAge(int age);
    public abstract void walkToPlace(String name);

}
